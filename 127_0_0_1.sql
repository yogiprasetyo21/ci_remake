-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 09, 2019 at 06:25 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `toko_jaya_abadi`
--
CREATE DATABASE IF NOT EXISTS `toko_jaya_abadi` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `toko_jaya_abadi`;

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `kode_barang` varchar(5) NOT NULL,
  `nama_barang` varchar(150) NOT NULL,
  `harga_barang` float NOT NULL,
  `kode_jenis` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `stok` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`kode_barang`, `nama_barang`, `harga_barang`, `kode_jenis`, `flag`, `stok`) VALUES
('BR001', 'iujjjuu', 5000, 'JS010', 1, 4),
('BR002', 'pensil', 2000, 'JS001', 1, 1),
('BR003', 'penghapus', 5000, 'JS001', 1, 2),
('BR004', 'pc', 5000000, 'JS002', 1, 1),
('BR005', 'mouse', 100000, 'JS002', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `kode_jabatan` varchar(5) NOT NULL,
  `nama_jabatan` varchar(50) NOT NULL,
  `keterangan` text NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jabatan`
--

INSERT INTO `jabatan` (`kode_jabatan`, `nama_jabatan`, `keterangan`, `flag`) VALUES
('JB011', 'Direktur', 'Operasional', 1),
('JB012', 'Big boss', 'Manager', 1),
('JB013', 'staff', 'Operasional', 1),
('JB014', 'staff', 'Operasional', 1),
('JB015', 'staff', 'Operasional', 1);

-- --------------------------------------------------------

--
-- Table structure for table `jenis_barang`
--

CREATE TABLE `jenis_barang` (
  `kode_jenis` varchar(5) NOT NULL,
  `nama_jenis` varchar(200) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jenis_barang`
--

INSERT INTO `jenis_barang` (`kode_jenis`, `nama_jenis`, `flag`) VALUES
('JS001', 'Alat Tulis kantor', 1),
('JS002', 'Perangkat lunak', 1);

-- --------------------------------------------------------

--
-- Table structure for table `karyawan`
--

CREATE TABLE `karyawan` (
  `nik` varchar(10) NOT NULL,
  `nama_lengkap` varchar(150) NOT NULL,
  `tempat_lahir` varchar(100) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `jenis_kelamin` varchar(1) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `kode_jabatan` varchar(5) NOT NULL,
  `flag` int(11) NOT NULL,
  `photo` varchar(150) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `karyawan`
--

INSERT INTO `karyawan` (`nik`, `nama_lengkap`, `tempat_lahir`, `tgl_lahir`, `jenis_kelamin`, `alamat`, `telp`, `kode_jabatan`, `flag`, `photo`) VALUES
('1904423', 'Reza akbar', 'jakarta', '1996-01-04', 'L', 'jl.kebon baru', '008721776777', 'BR001', 1, '20190422_1904423.jpg'),
('1904424', 'justin', 'surabaya', '0000-00-00', 'L', 'jl.sawah baru', '08989868765', 'BR001', 1, '20190422_1904424.jpg'),
('1904425', 'abdul bahar', 'jakarta', '0000-00-00', 'L', 'jl.kaki', '087666665654', 'JB00', 1, '20190422_1904425.jpg'),
('1904426', 'iqbal', 'jakarta', '2019-04-01', 'p', '                  jl.warakas 5 no 1', '00864718635', 'JB00', 1, '20190423_1904426.jpg'),
('1904427', 'Yogi p', 'sns', '2019-04-02', 'L', 'jl.jfjjf', '04848884', 'JB003', 1, '20190423_1904427.jpg'),
('1904428', 'ADIB', 'JDJJF', '2019-04-02', 'p', '                                    JL.DHHFJF', '089494994', 'BR001', 1, '20190423_1904428.jpg'),
('1904429', 'Bpk.relly', 'Bojonegoro', '1996-04-01', 'L', 'jl.dewa kembar', '089978773627', 'JB011', 1, '20190426_1904429.jpg'),
('1905430', 'Bayu bagas', 'surabaya', '1996-05-29', 'L', 'jl.warakas', '087665434345', 'JB014', 1, '20190502_1905430.jpg'),
('1905431', 'ahmad', 'bekasi', '2019-05-01', 'L', 'Jl.bekasi', '089998888', 'JB011', 1, 'default.jpg'),
('1905432', 'hamzah', 'bekasi', '2019-04-08', 'L', 'jl.bekasi', '089997989', 'JB012', 1, '20190509_1905432.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_detail`
--

CREATE TABLE `pembelian_detail` (
  `id_pembelian_d` int(11) NOT NULL,
  `id_pembelian_h` int(11) NOT NULL,
  `kode_barang` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_detail`
--

INSERT INTO `pembelian_detail` (`id_pembelian_d`, `id_pembelian_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(18, 20, 'BR002', 2, 5000000, 10000000, 1),
(20, 21, 'BR001', 2, 12000000, 24000000, 1),
(21, 21, 'BR003', 3, 15000000, 45000000, 1),
(22, 21, 'BR003', 6, 80000, 480000, 1),
(23, 21, 'BR004', 4, 80000, 320000, 1),
(24, 21, 'AS09', 10, 600, 6000, 1),
(25, 21, 'BR004', 7, 20000, 140000, 1),
(26, 22, 'BR001', 7, 500000, 3500000, 1),
(27, 22, 'BR003', 7, 20000000, 140000000, 1),
(28, 22, 'BR003', 7, 20000000, 140000000, 1),
(29, 34, 'BR003', 12, 50000000, 600000000, 1),
(30, 35, 'BR001', 9, 200000, 1800000, 1),
(31, 36, 'JP010', 7, 20000000, 140000000, 1),
(32, 32, 'AS09', 7, 200000, 1400000, 1),
(33, 32, 'JP001', 4, 200000, 800000, 1),
(34, 40, 'BR005', 4, 5000, 20000, 1),
(35, 41, 'BR005', 8, 9000, 72000, 1),
(36, 42, 'BR003', 10, 1000000, 10000000, 1),
(37, 43, 'BR001', 2, 7000, 14000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pembelian_header`
--

CREATE TABLE `pembelian_header` (
  `id_pembelian_h` int(11) NOT NULL,
  `no_transaksi` varchar(11) NOT NULL,
  `kode_supplier` varchar(5) NOT NULL,
  `tanggal` date NOT NULL,
  `approved` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian_header`
--

INSERT INTO `pembelian_header` (`id_pembelian_h`, `no_transaksi`, `kode_supplier`, `tanggal`, `approved`, `flag`) VALUES
(21, '19002', 'PG002', '2019-03-23', 1, 1),
(22, '19003', 'PG003', '2019-03-23', 1, 1),
(23, '19004', 'PG002', '2019-03-23', 1, 1),
(26, '19005', 'PG001', '2019-03-23', 1, 1),
(27, '19006', 'PG002', '2019-03-23', 1, 1),
(32, '3838', 'PG001', '2019-03-26', 1, 1),
(33, '19004', 'DT002', '2019-04-11', 1, 1),
(34, '12464', 'PG002', '2019-04-11', 1, 1),
(35, '12056', 'DT002', '2019-04-11', 1, 1),
(36, '12057', 'DT002', '2019-04-15', 1, 1),
(38, 'TR90416001', 'DT002', '2019-04-22', 1, 1),
(39, 'TR90416002', 'DT002', '2019-04-22', 1, 1),
(40, 'TR90413003', 'PG003', '2019-04-26', 1, 1),
(41, 'TR90521003', 'SP006', '2019-05-02', 1, 1),
(42, 'TR90521004', 'SP005', '2019-05-02', 1, 1),
(43, 'TR90523005', 'SP007', '2019-05-09', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_detail`
--

CREATE TABLE `penjualan_detail` (
  `id_jual_d` int(11) NOT NULL,
  `id_jual_h` int(11) NOT NULL,
  `kode_barang` varchar(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `harga` float NOT NULL,
  `jumlah` float NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_detail`
--

INSERT INTO `penjualan_detail` (`id_jual_d`, `id_jual_h`, `kode_barang`, `qty`, `harga`, `jumlah`, `flag`) VALUES
(2, 12349, 'BR004', 2, 100000, 200000, 1),
(3, 12349, 'BR005', 4, 400000, 1600000, 1),
(4, 12349, 'BR001', 4, 8000, 32000, 1),
(5, 12350, 'BR003', 1, 900000, 900000, 1),
(6, 12350, 'BR005', 6, 400000, 2400000, 1),
(7, 12351, 'BR003', 5, 900000, 4500000, 1),
(8, 12352, 'BR001', 8, 8000, 64000, 1),
(9, 12353, 'BR001', 8, 8000, 64000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `penjualan_header`
--

CREATE TABLE `penjualan_header` (
  `id_jual_h` int(11) NOT NULL,
  `no_transaksi` varchar(15) NOT NULL,
  `tanggal` date NOT NULL,
  `pembeli` varchar(250) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `penjualan_header`
--

INSERT INTO `penjualan_header` (`id_jual_h`, `no_transaksi`, `tanggal`, `pembeli`, `flag`) VALUES
(12349, '12345', '2019-04-24', 'Yogi', 1),
(12350, '13456', '2019-04-24', 'Ahmadu', 1),
(12352, 'TR90519001', '2019-05-02', 'Zaenal', 1),
(12353, 'TR90521002', '2019-05-02', 'zaky', 1);

-- --------------------------------------------------------

--
-- Table structure for table `supplier`
--

CREATE TABLE `supplier` (
  `kode_supplier` varchar(5) NOT NULL,
  `nama_supplier` varchar(100) NOT NULL,
  `alamat` text NOT NULL,
  `telp` varchar(15) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `supplier`
--

INSERT INTO `supplier` (`kode_supplier`, `nama_supplier`, `alamat`, `telp`, `flag`) VALUES
('SP004', 'Jack', 'jl.warakas no.9', '089666569', 1),
('SP005', 'Lili', 'jl.ganggeng no.10 D', '08977765544', 1),
('SP006', 'Ucok', 'jl.swasembada timur 22', '087666554435', 1),
('SP007', 'PT. maju jaya', 'jl.  soekarno', '0899789799', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `nik` varchar(10) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(32) NOT NULL,
  `tipe` int(11) NOT NULL,
  `flag` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id_user`, `nik`, `email`, `password`, `tipe`, `flag`) VALUES
(1, 'Admin', 'muhrinhajrin02@gmail.com', 'c93ccd78b2076528346216b3b2f701e6', 1, 1),
(2, '1704421300', 'rinhajrin0@gmail.com', 'cc63027830c1a0db0c6d6b69e91dde3b', 2, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`kode_barang`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`kode_jabatan`);

--
-- Indexes for table `jenis_barang`
--
ALTER TABLE `jenis_barang`
  ADD PRIMARY KEY (`kode_jenis`);

--
-- Indexes for table `karyawan`
--
ALTER TABLE `karyawan`
  ADD PRIMARY KEY (`nik`);

--
-- Indexes for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  ADD PRIMARY KEY (`id_pembelian_d`);

--
-- Indexes for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  ADD PRIMARY KEY (`id_pembelian_h`);

--
-- Indexes for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  ADD PRIMARY KEY (`id_jual_d`);

--
-- Indexes for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  ADD PRIMARY KEY (`id_jual_h`);

--
-- Indexes for table `supplier`
--
ALTER TABLE `supplier`
  ADD PRIMARY KEY (`kode_supplier`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `pembelian_detail`
--
ALTER TABLE `pembelian_detail`
  MODIFY `id_pembelian_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;
--
-- AUTO_INCREMENT for table `pembelian_header`
--
ALTER TABLE `pembelian_header`
  MODIFY `id_pembelian_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `penjualan_detail`
--
ALTER TABLE `penjualan_detail`
  MODIFY `id_jual_d` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `penjualan_header`
--
ALTER TABLE `penjualan_header`
  MODIFY `id_jual_h` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12354;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
