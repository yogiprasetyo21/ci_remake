<section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
        <img src="<?=base_url();?>assets/admin-lte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Yogi prasetyo</p>
          <a href="#"><i class="fa fa-circle text-success"></i>online</a>
        </div>
      </div>
      <!-- search form -->
      <form action="#" method="get" class="sidebar-form">
        <div class="input-group">
          <input type="text" name="q" class="form-control" placeholder="Search...">
          <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
           </span>
          </a>
        </li>
       <li class="treeview active">
          <a href="#">
            <i class="fa fa-folder"></i> <span>Data master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>karyawan/listkaryawan"><i class="fa fa-circle-o"></i>Data karyawan</a></li>
            <li><a href="<?=base_url();?>jabatan/listjabatan"><i class="fa fa-circle-o"></i>Data jabatan</a></li>
            <li><a href="<?=base_url();?>barang/listbarang"><i class="fa fa-circle-o"></i> Data barang</a></li>
            <li><a href="<?=base_url();?>jenis_barang/listjenisbarang"><i class="fa fa-circle-o"></i>Data jenis barang</a></li>
            <li><a href="<?=base_url();?>supplier/listsupplier"><i class="fa fa-circle-o"></i>Data supplier</a></li>
          </ul>
        </li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-pie-chart"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian/listpembelian"><i class="fa fa-circle-o"></i>pembelian</a></li>
            <li><a href="<?=base_url();?>Penjualan/listpenjualan"><i class="fa fa-circle-o"></i> Penjualan</a></li>
          </ul>
        </li>
        <li class="treeview">   
          <a href="#">
            <i class="fa fa-laptop"></i>
            <span>Report</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?=base_url();?>pembelian/dtp"><i class="fa fa-circle-o"></i>Pembelian</a></li>
            <li><<a href="<?=base_url();?>Penjualan/reportjual"><i class="fa fa-circle-o"></i>Penjualan</a></li>
          </ul>
        </li>
        
    </section>