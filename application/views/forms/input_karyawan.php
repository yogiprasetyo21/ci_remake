                  <div style="color: red" align="center"><?= validation_errors(); ?></div>
      <form action="<?=base_url()?>karyawan/inputkaryawan" method="POST" enctype="multipart/form-data">
    
          <!-- general form elements -->
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Input Karyawan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
        
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Nik</label>
                  <input type="text" class="form-control" value ="<?=$nik_baru;?>" name="nik" id="nik" value="<?=set_value('nik');?>" maxlength="10" readonly>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Karyawan</label>
                  <input type="text" class="form-control" name="nama_lengkap" id="nama_lengkap" value="<?=set_value('nama_lengkap');?>" maxlength="50" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Tempat lahir</label>
                 <input type="text" class="form-control" name="tempat_lahir" id="tempat_lahir" value="<?=set_value('tempat_lahir');?>" maxlength="50" autocomplete="off">
               </div>

                <div class="form-group">
                <meta charset="utf-8">
                <meta name="viewport" content="width=device-width, initial-scale=1">
                <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
                <link rel="stylesheet" href="/resources/demos/style.css">
                <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
                <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
                <script>
                $( function() {
                  $( "#tgl_lahir" ).datepicker({dateFormat : "yy-mm-dd"});
                
                } );
                </script>
                  <label>Tanggal Lahir</label>
                  <td width="179">
                   <input type="text" class="form-control" name="tgl_lahir" id="tgl_lahir"  
                   value="<?=set_value('tgl_lahir');?>" autocomplete="off">
                 </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Jenis kelamin</label>
                  <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
                 <option value="L">Laki laki</option>
                 <option value="P">Perempuan</option>
                
                </select>

                </div>
                    <div class="form-group">
                  <label for="exampleInputPassword1">Telepon</label>
                  <input type="text" class="form-control" name="telp" id="telp"
                  value="<?=set_value('telp');?>" autocomplete="off">

                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Alamat</label>
                  <textarea name="alamat" class="form-control" id="alamat" cols="45" rows="3"><?=set_value('alamat');?></textarea>

                </div>  

                <div class="form-group">
                  <label for="exampleInputPassword1">Jabatan</label>
                  <select name="kode_jabatan" id="kode_jabatan" class="form-control">
                <?php foreach($data_jabatan as $data) {?>
                   <option value="<?= $data->kode_jabatan;?>">
                   <?= $data->nama_jabatan; ?></option>
                   
                  
                  <?php }?>
                  
                </select>


                </div>
                <div class="form-group">
                  <label for="exampleInputFile">Upload foto</label>
                      <input type="file" name="image" id="image"/>
                  </div>
                    
                    <input type="submit" name="simpan" id="simpan"class="btn btn-info" value="simpan">
      <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya"></a>

              
              
  </form>