

 
 <?php
foreach ($detail_barang as $data) {
  $kode_barang  = $data->kode_barang;
  $nama_barang  = $data->nama_barang;
  $harga_barang  = $data->harga_barang;
  $kode_jenis  = $data->kode_jenis;
  $stok  = $data->stok;
}

?>
       <div style="color: red" align="center"><?= validation_errors(); ?></div>
       <form action="<?=base_url()?>barang/editbarang/<?= $kode_barang; ?>" method="POST">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Barang</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
        
              <div class="box-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Kode barang</label>
                <input type="text" name="kode_barang" id="kode_barang" class="form-control" maxlength="10" value ="<?=$kode_barang;?>" readonly>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Nama Barang</label>
                   
                 <input type="text" class="form-control"name="nama_barang" id="nama_barang" value ="<?=$nama_barang;?>" autocomplete="off">
                  </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Harga</label>
                  <input type="text" name="harga_barang" id="harga_barang" class="form-control" value ="<?=$harga_barang;?>" autocomplete="off">
               </div>

                <div class="form-group">
                <label for="exampleInputEmail1">Nama jenis</label>
               <select name="kode_jenis" id="kode_jenis" class="form-control" value ="<?=$kode_jenis ;?>" >

    <?php foreach($data_jenis_barang as $data) {
    $select_jenis = ($data->kode_jenis == $kode_jenis) ? 'selected' : '';
    ?>
       <option value="<?= $data->kode_jenis;?>" <?=$select_jenis; ?>>
       <?= $data->nama_jenis; ?></option>
       
      
      <?php }?>
      
      </select>
                  </div>

                <div class="form-group">
                <label for="exampleInputEmail1">Stok</label>
               <input type="text" name="stok" id="stok" class="form-control" value ="<?=$stok ;?>"  autocomplete="off">
                    
      <input type="submit" name="simpan" id="simpan" class="btn btn-info" value="simpan" style="background-color:#06F">
      <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset">
      <br></br>
      <a href="<?=base_url();?>barang/listbarang"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya"></a>
  
              
              
  </form>