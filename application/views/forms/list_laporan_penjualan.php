 </br></br> 
<center><h3>Laporan Data Penjualan Dari Tanggal</h3></center>
  <center><h4><?=$tgl_awal;?> s/d <?=$tgl_akhir;?></h4></center>
</br>
<table width="93%">
  <tr>
    <td width="552"><div align="left"><a href="<?=base_url();?>penjualan/reportjual"><input type="submit" name="kembali" value="Kembali"></a>
      
    </div>
    <td width="552"><div align="right"><a href="<?=base_url();?>penjualan/cetak/<?= $tgl_awal; ?>/<?= $tgl_akhir ;?>"><input type="submit" name="cetak" value="Cetak PDF"></a></div></td></td>
  </tr>
<table width="93%" cellpadding="7" align="center">
<tr align="center" >
    <th>No</th>
    <th>ID Penjualan</th>
    <th>Nomor Transaksi</th>
   	<th>Tanggal</th>
    <th>Total Barang</th>
    <th>Total Qty</th>
    <th>Jumlah Nominal Penjualan</th>
    

 </tr>
  <?php
  $no = 0;
   $total_keseluruhan = 0;

    foreach ($data_penjualan_detail as $data) {
		$no++;

?>
<tr>
   
    <td><?=$no;?></td>
    <td><?= $data->id_jual_h; ?></td>
	  <td><?= $data->no_transaksi; ?></td>
    <td><?= $data->tanggal; ?></td>
    <td><?= $data->total_barang; ?></td>
    <td><?= $data->total_qty; ?></td>
    <td>RP. <?= number_format($data->total_penjualan); ?></td>
</tr>
<?php 
		//menghitung total
		$total_keseluruhan+= $data->total_penjualan;
	}
?>
</table>

<table width="93%" cellspacing="0" cellpadding="7" align="center">
<tr bgcolor="#00FFFF">
<td>Total Keseluruhan Penjualan &emsp; Rp.<?=number_format($total_keseluruhan); ?></td>
 </tr>
 </table>
  
