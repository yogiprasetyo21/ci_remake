
        <div class="box-header with-border">
              <h3 class="box-title">Input Barang</h3>
            </div>
            <center><div style="color: red"><?= validation_errors(); ?></div>
            </center>
            <form action="<?=base_url()?>barang/inputbarang" method="POST">

              <div class="box-body">
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputEmail1">Kode Barang</label>
                 <input type="text" class="form-control" value ="<?=$data_barang;?>" name="kode_barang" id="kode_barang" value="<?= set_value('kode_barang');?>" readonly >
                </div>
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Nama Barang</label>
                   <input type="text" class="form-control"name="nama_barang" id="nama_barang" value="<?= set_value('nama_barang');?>" autocomplete="off" placeholder="Nama Barang">
                </div>
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Harga</label>
                  <input type="text" class="form-control"name="harga_barang" id="harga_barang"value="<?= set_value('harga_barang');?>" autocomplete="off" placeholder="Harga Barang">
                </div>
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Nama Jenis</label>
                  <select name="kode_jenis" id="kode_jenis" class="form-control">
            
            <?php foreach($data_jenis_barang as $data) {?>
            <option value="<?= $data->kode_jenis;?>">
            <?= $data->kode_jenis; ?>
            <?= $data->nama_jenis; ?> 
             </option>
       
      
      <?php }?>
      
      </select>
                </div>
                <div class="form-group" style="width: 50% ">
                  <label for="exampleInputPassword1">Stok</label>
                  <input type="text" class="form-control" name="stok" id="stok" 
                  value="<?= set_value('stok');?>" placeholder="Stok" autocomplete="off">
                <div class="checkbox">
                  
                </div>
              </div>
              <!-- /.box-body -->

              <div class="box-footer">
                
                  <input type="submit" value="Proses" class="btn btn-info" name="simpan"> 
        
                <input type="submit" name="batal" id="batal" class="btn btn-info" value="reset"> 
                  <br></br>

              </div>
            </form>
          