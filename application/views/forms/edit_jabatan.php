
 <?php
foreach ($detail_jabatan as $data) {
	$kode_jabatan  = $data->kode_jabatan;
	$nama_jabatan  = $data->nama_jabatan;
	$keterangan  = $data->keterangan;
}
?>
       <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>jabatan/editjabatan/<?= $kode_jabatan; ?>" method="POST">
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Edit Jabatan</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
        
              <div class="box-body">
                <div class="form-group">
                    <label for="exampleInputEmail1">Kode jabatan</label>
                    <input type="text" name="kode_jabatan" id="kode_jabatan" class="form-control" maxlength="5" value ="<?=$kode_jabatan;?>"  value="<?= set_value('kode_jabatan');?>" readonly> 
                </div>
                <div class="form-group">
                      <label for="exampleInputPassword1">Nama Jabatan</label>
                     <input type="text" name="nama_jabatan" id="nama_jabatan" class="form-control" value ="<?=$nama_jabatan;?>" value="<?= set_value('nama_jabatan');?>" autocomplete="off">
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword1">Keterangan</label>
        <select name="keterangan" id="keterangan" class="form-control" value ="<?=$keterangan;?>" value="<?= set_value('keterangan');?>" />
       <option value="Operasional">Operasional</option>
     <option value="Manager">Manager</option>
      
    </select>     
      </div>
                    
      <input type="submit" name="simpan" id="simpan" class="btn btn-info" value="simpan">
      <input type="submit" name="batal" id="batal" class="btn btn-info">
      <br></br>
      <a href="<?=base_url();?>jabatan/listjabatan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" class="btn btn-info" value="kembali ke menu sebelumnya"></a></td>
              
              
  </form>