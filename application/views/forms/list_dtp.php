
<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#tgl_awal" ).datepicker({dateFormat : "yy-mm-dd"});
  $( "#tgl_akhir" ).datepicker({dateFormat : "yy-mm-dd"});
  
  } );
  </script>
<script>
  $(document).ready(function(){

    $('#proses').on('click', function(event) {
    event.preventDefault();
    var tgl_awal = $('#tgl_awal').val();
    var tgl_akhir = $('#tgl_akhir').val();

    if (tgl_awal == '' || tgl_akhir == '') {
      alert('Tanggal Tidak Boleh Kosong');
    }else if (new Date(tgl_awal) > new Date(tgl_akhir)) {
      alert('Format Waktu Salah Input');
    }else{
      $('#forms').submit();
    }
    });

  });
</script>

 <div class="box-header">
              <h3 class="box-title">Report pembelian</h3>
            </div>
            <div class="box-body">
            
              <form method="POST" action="<?=base_url();?>pembelian/laporanPembelian" 
                name="forms" id="forms">
              <div class="form-group">
                <label>Tanggal Awal</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                 <input type="text" class="form-control" name="tgl_awal" id="tgl_awal" placeholder="Tanggal Awal..." autocomplete="off">
                  </div></br></br>

                 <div class="form-group">
                <label>Tanggal Akhir</label>

                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                <input type="text" class="form-control" name="tgl_akhir" id="tgl_akhir" placeholder="Tanggal Akhir..." autocomplete="off">



            </div></br>
                          <tr align="center">
                        <td><input type="submit" name="proses" id="proses" class="btn btn-info"  
                           value="proses"/></td>
                      </tr>
                    </form>
  