<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jenis_barang extends CI_controller {

	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("jenis_barang_model");

		//cek sesi login
		$user_login = $this->session->userdata();
		if(count($user_login) <= 1){
			redirect("auth/index", "refresh");
		}
	
	}
	
	public function index()
	
	{
		$this->listjenisBarang();
	}
	
	public function listjenisBarang()
	
	{   

		if (isset($_POST['tombol_cari'])) {
			$data['kata_pencarian'] = $this->input->post('caridata');
			$this->session->set_userdata('session_pencarian', $data['kata_pencarian']);
		}else{
			$data['kata_pencarian'] = $this->session->userdata('session_pencarian');
			
		}
		
		$data['data_jenis_barang'] = $this->jenis_barang_model->tombolpagination($data['kata_pencarian']);

		//$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['content']       ='forms/list_jenis_barang';
		$this->load->view('home_2', $data);
	}

	public function inputjenisbarang()
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['data_jenis_barang'] = $this->jenis_barang_model->createKodeUrut();
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
			
		if ($validation->run()){
			$this->jenis_barang_model->save();
		   $this->session->set_flashdata('info', '<div style="color: green"> SIMPAN DATA BERHASIL! </div>');
				redirect("jenis_barang/index", "refresh");

		}
			
		//$this->load->view('input_jenis_barang');
		$data['content']       ='forms/input_jenis_barang';
		$this->load->view('home_2', $data);
	}
	public function detailjenisbarang($kode_jenis)
	
	{
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$data['content']       ='forms/detail_jenis_barang';
		$this->load->view('home_2', $data);
	}
	
	public function editjenisbarang($kode_jenis)
	{
 
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);

		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
			
		if ($validation->run()){
			$this->jenis_barang_model->update($kode_jenis);
		   $this->session->set_flashdata('info', '<div style="color: green"> EDIT DATA BERHASIL! </div>');
				redirect("jenis_barang/index", "refresh");
		}
			
		//$this->load->view('edit_jenis_barang', $data);
		$data['content']       ='forms/edit_jenis_barang';
		$this->load->view('home_2', $data);
	}

   public function deletejenisbarang($kode_jenis)
   {
	   		$m_jenis_barang = $this->jenis_barang_model;
			$m_jenis_barang->delete($kode_jenis);
			redirect("jenis_barang/index", "refresh");
	}

}
